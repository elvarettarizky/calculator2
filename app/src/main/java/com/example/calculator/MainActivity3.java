package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity3 extends AppCompatActivity implements View.OnClickListener {

    TextView tvOperasi, tvHasil;
    Button btback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        tvOperasi = findViewById(R.id.tvOperasi);
        tvHasil = findViewById(R.id.tvHasil);
        btback = findViewById(R.id.btback);

        btback.setOnClickListener(this);

        Intent intent = getIntent();
//        String tampilanOperasi = ;
//        String hasilOperasi = ;

        tvOperasi.setText(intent.getStringExtra("tampilanOperasi"));
        tvHasil.setText(intent.getStringExtra("hasilOperasi"));
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btback) {
            finish();
        }

    }
}